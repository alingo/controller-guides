# controller-guides
SVG guides for game controllers \ keyboards

There are currently four guides:

1. A Logitech G12 guide

2. An X-Arcade Solo guide

3. A Saitek X52 Pro guide

4. A Steam Controller guide


They may not appear correctly via GitHub's SVG -> image renderer.
